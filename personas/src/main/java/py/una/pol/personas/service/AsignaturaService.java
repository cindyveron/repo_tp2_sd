/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre() + " " + a.getProfesor());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre() + " " + a.getProfesor() );
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorId(long id) {
    	return dao.seleccionarPorId(id);
    }
    
    public long borrar(long id) throws Exception {
    	return dao.borrar(id);
    }
    
    public void modificar(Asignatura a) throws Exception {
        log.info("Modificando Asignatura: " + a.getNombre() + " " + a.getProfesor());
        try {
        	dao.actualizar(a);
        }catch(Exception e) {
        	log.severe("ERROR al modificar asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura modificada con éxito: " + a.getNombre() + " " + a.getProfesor() );
    }
    
    public void asociar(long codigo, long cedula) throws Exception {
        log.info("Asociando: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
        try {
        	dao.asociar(codigo, cedula);
        }catch(Exception e) {
        	log.severe("ERROR al asociar persona con asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Asociacion con éxito: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
    }
    
    public void desasociar(long codigo, long cedula) throws Exception {
        log.info("Desasociando: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
        try {
        	dao.desasociar(codigo, cedula);
        }catch(Exception e) {
        	log.severe("ERROR al desasociar persona con asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Desasociacion con éxito: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
    }
    
    
    public List<Asignatura> listarAsignaturaPorPersona(long cedula) {
    	return dao.listarAsignaturaPorPersona(cedula);
    }
    
    public List<Persona> listarPersonaPorAsignatura(long id) {
    	return dao.listarPersonaPorAsignatura(id);
    }
    
    
    
}
